<?php
defined('TYPO3_MODE') || die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

(function ($extension = 'headertoallelements' ) {
    ExtensionManagementUtility::addTypoScript(
        $extension,
        'setup',
        '@import \'EXT:'.$extension.'/Configuration/TypoScript/setup.typoscript\'',
        'defaultContentRendering'
    );

    ExtensionManagementUtility::addTypoScript(
        $extension,
        'constants',
        '@import \'EXT:'.$extension.'/Configuration/TypoScript/constants.typoscript\'',
        'defaultContentRendering'
    );
})();
