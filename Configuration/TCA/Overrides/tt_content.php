<?php
defined('TYPO3_MODE') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(function () {
    $extensionKey = 'headertoallelements';

    $additionalColumns = [
        'disableShortcutHeadline' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.headline',
            'config' => [
                'type' => 'check',
            ],
        ],
        'disableShortcutReadmore' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.readmore',
            'config' => [
                'type' => 'check',
            ],
        ],
        'disableToTopLink' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.totoplink',
            'config' => [
                'type' => 'check',
            ],
        ],
    ];

    ExtensionManagementUtility::addTCAcolumns('tt_content', $additionalColumns);

    if (ExtensionManagementUtility::isLoaded('readmorelink')) {
        $GLOBALS['TCA']['tt_content']['palettes']['shortcutheader']['showitem'] = '
            header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
            --linebreak--,
            header_layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel,
            header_position;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_position_formlabel,
            date;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:date_formlabel,
            --linebreak--,
            header_link;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel,
            readmorelinkactive;LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.activate.label,
            readmorelinktext;LLL:EXT:readmorelink/Resources/Private/Language/locallang.xlf:plugin.mdy.readmorelink.be.string.override.label,
            --linebreak--,
            disableShortcutHeadline;LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.headline,
            disableShortcutReadmore;LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.readmore,
            disableToTopLink;LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.totoplink,
            --linebreak--,
            subheader;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:subheader_formlabel
        ';

        $GLOBALS['TCA']['tt_content']['types']['shortcut']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.shortcutheader;shortcutheader,records;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:records_formlabel,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--palette--;LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.palette.ttcontent.title;contentadditionalwrapcontent,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        $GLOBALS['TCA']['tt_content']['types']['html']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext.ALT.html_formlabel,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--palette--;LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.palette.ttcontent.title;contentadditionalwrapcontent,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
    } else {
        $GLOBALS['TCA']['tt_content']['palettes']['shortcutheader']['showitem'] = '
            header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
            --linebreak--,
            header_layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel,
            header_position;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_position_formlabel,
            date;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:date_formlabel,
            --linebreak--,
            header_link;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel,
            --linebreak--,
            disableShortcutHeadline;LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.headline,
            disableToTopLink;LLL:EXT:' . $extensionKey . '/Resources/Private/Language/locallang.xlf:plugin.mdy.' . $extensionKey . '.be.string.disable.shortcut.totoplink,
            --linebreak--,
            subheader;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:subheader_formlabel
        ';

        $GLOBALS['TCA']['tt_content']['types']['shortcut']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.shortcutheader;shortcutheader,records;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:records_formlabel,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--palette--;LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.palette.ttcontent.title;contentadditionalwrapcontent,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
        $GLOBALS['TCA']['tt_content']['types']['html']['showitem'] = '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext.ALT.html_formlabel,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,--palette--;LLL:EXT:contentadditionalwrap/Resources/Private/Language/locallang.xlf:plugin.mdy.contentadditionalwrap.be.string.palette.ttcontent.title;contentadditionalwrapcontent,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,--div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended';
    }
});