#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
  disableShortcutReadmore tinyint(3) DEFAULT '0' NOT NULL,
  disableShortcutHeadline tinyint(3) DEFAULT '0' NOT NULL,
  disableToTopLink tinyint(3) DEFAULT '0' NOT NULL,
);