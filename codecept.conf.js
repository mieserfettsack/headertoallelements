const {setHeadlessWhen} = require('@codeceptjs/configure');

setHeadlessWhen(process.env.HEADLESS);

exports.config = {
    tests: './Tests/CodeceptJS/Tests/*_test.js',
    output: './Tests/CodeceptJS/Output',
    helpers: {
        SVGClick: {
            require: './Tests/CodeceptJS/Helper/SVGClick_helper.js',
        },
        Puppeteer: {
            "chrome": {
                "args": ["--no-sandbox"]
            },
            url: 'http://headertoallelements.ddev.site/',
            show: false,
            windowSize: '1200x900',
            waitForTimeout: 2000,
            waitForAction: 250
        }
    },
    include: {
        I: './Tests/CodeceptJS/steps_file.js'
    },
    bootstrap: null,
    mocha: {},
    name: 'html',
    plugins: {
        retryFailedStep: {
            enabled: true
        },
        screenshotOnFail: {
            enabled: true
        }
    }
};
