# headertoallelements

This TYPO3 extensions adds header pallets to HTML and shortcut content elements. The editor can even decide which header 
to use when using shortcut element. 

## Installation

This extension only works with TYPO3. Pick one of the following ways to install this extension to your TYPO3 page.

### Install via composer

`composer require mdy/headertoallelements`

### Install via TYPO3 Extension Manager

Search for headertoallelements and install the extension. Make sure the extension is activated after installation.

### Install via release downloads

Go to https://gitlab.com/mieserfettsack/headertoallelements/-/tags and click the download icon on one of the releases available.
Download the archive, extract it and upload it to your TYPO3 via FTP, SSH or whatever.

## Configuration

There is no configuration needed. THis extension run automatically after installation.