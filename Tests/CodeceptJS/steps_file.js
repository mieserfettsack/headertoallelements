// in this file you can append custom step methods to 'I' object

module.exports = function() {
  return actor({
    loginBackend: function(){
      I = this;

      I.amOnPage('http://headertoallelements.ddev.site/typo3/');
      I.waitForElement('#t3-username');
      I.fillField('#t3-username', 'admin');
      I.fillField('#t3-password', secret('adminadmin'));
      I.pressKey('Enter');
      I.waitForElement('#web_layout', 10);
    },

    seeBodytextInSelector: function (selector) {
      I = this;

      I.see('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', selector);
    }
  });
};
