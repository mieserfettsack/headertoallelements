const { Helper } = codeceptjs;
const assert = require('assert');

class SVGClick extends Helper {
  async clickOnCoordinate(coordinates) {
    const page = this.helpers['Puppeteer'].page;
    page.mouse.click(coordinates[0], coordinates[1]);
  }
}

module.exports = SVGClick;
