Feature('Test functionality in backend');

Scenario('Precache and test availability of test pages', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/');
    I.amOnPage('http://headertoallelements.ddev.site/typo3/');
});

Scenario('header for HTML element is working', (I) => {
    I.loginBackend();
    I.click('#web_layout');
    I.clickOnCoordinate([317,158]);
    I.wait(2);
});
