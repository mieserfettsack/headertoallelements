Feature('Test functionality in frontend');

Scenario('Precache and test availability of test pages', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element');
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element-and-different-appearance');
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element-header-and-subheader');
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element-header-subheader-and-disable-included-content');
    I.amOnPage('http://headertoallelements.ddev.site/html-content-element');
});

Scenario('Shortcut element', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element');
    I.see('Header external element', 'header > h2');
    I.see('Subheader external element', 'header > h3');
    I.see('Included content');
    I.seeBodytextInSelector('.ce-bodytext > p');
});

Scenario('HTML content element', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/html-content-element');
    I.see('Headline', 'header > h2');
    I.see('Subheader', 'header > h3');
    I.seeBodytextInSelector('.frame-type-html > p');
});

Scenario('Shortcut element and different appearance', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element-and-different-appearance');
    I.seeElementInDOM('.frame-ruler-before.frame-type-shortcut.frame-layout-1.frame-space-before-extra-small.frame-space-after-extra-small');
});

Scenario('Shortcut element, header and subheader', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element-header-and-subheader');
    I.see('Header', '.frame-shortcut-wrap > div > header > h2');
    I.see('Subheader', '.frame-shortcut-wrap > div > header > h3');
    I.see('Header external element', '.frame-shortcut-wrap > div > div > header > h2');
    I.see('Subheader external element', '.frame-shortcut-wrap > div > div > header > h3');
    I.see('Included content');
    I.seeBodytextInSelector('.ce-bodytext > p');
    I.seeElementInDOM('.frame-toTopLink');
});

Scenario('Shortcut element, header, subheader and disable included content', (I) => {
    I.amOnPage('http://headertoallelements.ddev.site/shortcut-element-header-subheader-and-disable-included-content');
    I.see('Header', '.frame-shortcut-wrap > div > header > h2');
    I.see('Subheader', '.frame-shortcut-wrap > div > header > h3');
    I.dontSeeElementInDOM('.frame-shortcut-wrap > div > div > header > h2');
    I.dontSeeElementInDOM('.frame-shortcut-wrap > div > div > header > h3');
    I.see('Included content');
    I.seeBodytextInSelector('.ce-bodytext > p');
    I.dontSeeElementInDOM('.frame-toTopLink');
});
