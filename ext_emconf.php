<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'headertoallelements',
    'description' => 'Adds the headers palette to tt_content.html and tt_content.shortcut',
    'category' => 'fe',
    'author' => 'Christian Stern',
    'author_email' => 'christian.stern@krummenhak.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '1.2.4',
    'constraints' => array(
        'depends' => array(
            'typo3' => '9.4.0-11.5.99'
        )
    )
);

